﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_03_console
{
    class Program
    {
        static string q1 = "please select what list of things you would like to look at.";
        static string q2 = "[0]animals [1]cars [2]food [3]linux distros [4]EXIT";
        static void Main(string[] args)
        {
            //change output to the user depending on what option they choose
            //user needs to be able to choose from a set of options.
            //each of these options will change a label or console.writeline to the user
            
            while (true)
            {
                
                Console.Clear();
                Console.WriteLine(q1);
                Console.WriteLine(q2);
                var choice = int.Parse(Console.ReadLine());
                if (choice < 4)
                {
                    Console.WriteLine($"{list(choice)}");
                    Console.WriteLine($"Press <ENTER> when you are done");
                    Console.ReadKey();
                }
                else
                {
                    break;
                }
            }
        }

        static string list(int choice)
        {
            var output = "";
            
            switch (choice)
            {
                case 0:
                    Console.Clear();
                    output = "cat, dog, horse, gorilla";
                    break;
                case 1:
                    Console.Clear();
                    output = "nissan tiida, nissan skyline, nissan pulsar, subaru impreza";
                    break;
                case 2:
                    Console.Clear();
                    output = "black beans, garlic cheese toasty, noodles, meatball sub of the day";
                    break;
                case 3:
                    Console.Clear();
                    output = "gentoo, debian, arch, mint";
                    break;
                case 4:
                    break;
            }
            return output;
            
        }
    }
}
