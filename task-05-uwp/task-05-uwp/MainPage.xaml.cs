﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace task_05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static int points = 0;
        static int random = 0;
        static int turns = 0;

        static string info1 = "Guess a number between 1 and 5";
        static string info2 = "you get 5 turns, see how many you can guess";
        static string q1 = "Enter your guess";

        public MainPage()
        {
            this.InitializeComponent();

            in1.Text = info1;
            in2.Text = info2;
            question1.Text = q1;
        }

        private void check_Click(object sender, RoutedEventArgs e)
        {
            if (turns < 4)
            {
                output.Text = correctornot(int.Parse(ui.Text));
                score.Text = $"you have a score of {points} well done";
            }
            else
            {
                check.Visibility = Visibility.Collapsed;
                output.Text = correctornot(int.Parse(ui.Text));
                score.Text = $"you got a score of {points} well done";
            }

        }

        static int randomnum(int random)
        {
            Random rnd = new Random();
            int answer = rnd.Next(1, 6);

            return answer;
        }

        static string correctornot(int check)
        {
            var output = "";
            var rand = randomnum(random);

            if (randomnum(random) == check)
            {
                points = points + 1;
                output = $"you guessed {check} and it was {randomnum(random)}.";

            }
            else
            {
                output = $"you guessed {check} and it was {randomnum(random)}.";
            }
            turns++;
            return output;


        }
    }
}
