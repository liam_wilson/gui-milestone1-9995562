﻿namespace task_01_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.question1 = new System.Windows.Forms.Label();
            this.ui = new System.Windows.Forms.TextBox();
            this.question2 = new System.Windows.Forms.Label();
            this.choice = new System.Windows.Forms.ComboBox();
            this.answer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // question1
            // 
            this.question1.AutoSize = true;
            this.question1.Location = new System.Drawing.Point(151, 23);
            this.question1.Name = "question1";
            this.question1.Size = new System.Drawing.Size(35, 13);
            this.question1.TabIndex = 0;
            this.question1.Text = "label1";
            // 
            // ui
            // 
            this.ui.Location = new System.Drawing.Point(154, 61);
            this.ui.Name = "ui";
            this.ui.Size = new System.Drawing.Size(100, 20);
            this.ui.TabIndex = 1;
            // 
            // question2
            // 
            this.question2.AutoSize = true;
            this.question2.Location = new System.Drawing.Point(154, 120);
            this.question2.Name = "question2";
            this.question2.Size = new System.Drawing.Size(35, 13);
            this.question2.TabIndex = 2;
            this.question2.Text = "label1";
            // 
            // choice
            // 
            this.choice.FormattingEnabled = true;
            this.choice.Items.AddRange(new object[] {
            "0",
            "1"});
            this.choice.Location = new System.Drawing.Point(205, 153);
            this.choice.Name = "choice";
            this.choice.Size = new System.Drawing.Size(121, 21);
            this.choice.TabIndex = 3;
            this.choice.SelectedIndexChanged += new System.EventHandler(this.choice_SelectedIndexChanged);
            // 
            // answer
            // 
            this.answer.AutoSize = true;
            this.answer.Location = new System.Drawing.Point(145, 213);
            this.answer.Name = "answer";
            this.answer.Size = new System.Drawing.Size(35, 13);
            this.answer.TabIndex = 4;
            this.answer.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 390);
            this.Controls.Add(this.answer);
            this.Controls.Add(this.choice);
            this.Controls.Add(this.question2);
            this.Controls.Add(this.ui);
            this.Controls.Add(this.question1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label question1;
        private System.Windows.Forms.TextBox ui;
        private System.Windows.Forms.Label question2;
        private System.Windows.Forms.ComboBox choice;
        private System.Windows.Forms.Label answer;
    }
}

