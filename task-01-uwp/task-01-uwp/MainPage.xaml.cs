﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace task_01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "enter the amount of distance you want to convert";
        static string q2 = "Do you want to convert it to [0]miles or [1]kilometers?";
        

        public MainPage()
        {
            this.InitializeComponent();

            question1.Text = q1;
            question2.Text = q2;
        }

        private void choice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            answer.Text = choices(ui.Text, choice.SelectedIndex);
        }

        static string choices(string units, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"{units} Kilometers = {convertmile(double.Parse(units))} Miles";
                    break;
                case 1:
                    answer = $"{units} Miles = {convertkm(double.Parse(units))} Kilometers";
                    break;
                default:
                    answer = $"Not a valid value was given";
                    break;
            }

            return answer;
        }

        static double convertmile(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var output = mile * converter;

            output = System.Math.Round(output, 2);
            return output;
        }

        static double convertkm(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var output = km * converter;

            output = System.Math.Round(output, 2);
            return output;
        }
    }
}
