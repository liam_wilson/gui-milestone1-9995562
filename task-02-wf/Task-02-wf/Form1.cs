﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_02_wf
{
    public partial class Form1 : Form
    {
        static string q1 = "enter the prices of grocery items, your total will be displayed when you press add.";
        static string q2 = "you can keep adding prices and the total will keep going up";
        static double price = 0.00;
        static double total = 0.00;
        public Form1()
        {
            InitializeComponent();
            question1.Text = q1;
            question2.Text = q2;

        }

        private void additem_Click(object sender, EventArgs e)
        {
            price = double.Parse(ui.Text);
            Answer.Text = addingtototal(price);
        }

        static string addingtototal(double price)
        {
            var answer = "";
            total = total + price;
            answer = $"The total cost is ${addGst(total)}";
            return answer;
        }

        static string addGst(double total)
        {
            var output = "";
            total = total * 1.15;
            total = System.Math.Round(total, 2);
            output = Convert.ToString(total);
            return output;
        }
    }
}
