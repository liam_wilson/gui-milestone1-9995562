﻿namespace task_02_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.question1 = new System.Windows.Forms.Label();
            this.ui = new System.Windows.Forms.TextBox();
            this.additem = new System.Windows.Forms.Button();
            this.Answer = new System.Windows.Forms.Label();
            this.question2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // question1
            // 
            this.question1.AutoSize = true;
            this.question1.Location = new System.Drawing.Point(12, 34);
            this.question1.Name = "question1";
            this.question1.Size = new System.Drawing.Size(29, 13);
            this.question1.TabIndex = 0;
            this.question1.Text = "label";
            // 
            // ui
            // 
            this.ui.Location = new System.Drawing.Point(15, 68);
            this.ui.Name = "ui";
            this.ui.Size = new System.Drawing.Size(100, 20);
            this.ui.TabIndex = 1;
            // 
            // additem
            // 
            this.additem.Location = new System.Drawing.Point(185, 64);
            this.additem.Name = "additem";
            this.additem.Size = new System.Drawing.Size(75, 23);
            this.additem.TabIndex = 2;
            this.additem.Text = "Add";
            this.additem.UseVisualStyleBackColor = true;
            this.additem.Click += new System.EventHandler(this.additem_Click);
            // 
            // Answer
            // 
            this.Answer.AutoSize = true;
            this.Answer.Location = new System.Drawing.Point(15, 124);
            this.Answer.Name = "Answer";
            this.Answer.Size = new System.Drawing.Size(0, 13);
            this.Answer.TabIndex = 3;
            // 
            // question2
            // 
            this.question2.AutoSize = true;
            this.question2.Location = new System.Drawing.Point(15, 49);
            this.question2.Name = "question2";
            this.question2.Size = new System.Drawing.Size(35, 13);
            this.question2.TabIndex = 4;
            this.question2.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 276);
            this.Controls.Add(this.question2);
            this.Controls.Add(this.Answer);
            this.Controls.Add(this.additem);
            this.Controls.Add(this.ui);
            this.Controls.Add(this.question1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label question1;
        private System.Windows.Forms.TextBox ui;
        private System.Windows.Forms.Button additem;
        private System.Windows.Forms.Label Answer;
        private System.Windows.Forms.Label question2;
    }
}

