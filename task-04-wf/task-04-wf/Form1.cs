﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_04_wf
{
    public partial class Form1 : Form
    {
        static string q1 = "Hello there welcome to the fruit and vege market!";
        static string q2 = "Tell me what you want and I'll check if we have it.";
        public Form1()
        {
            InitializeComponent();
            question1.Text = q1;
            question2.Text = q2;
        }

        private void check_Click(object sender, EventArgs e)
        {
            answer.Text = illcheckman(ui.Text);
        }

        static string illcheckman(string check)
        {
            var output = "";
            var produce = new Dictionary<string, string>();

            produce.Add("mandarin", "fruit");
            produce.Add("mandarins", "fruit");
            produce.Add("mango", "fruit");
            produce.Add("mangos", "fruit");
            produce.Add("mangoes", "fruit");
            produce.Add("kiwifruit", "fruit");
            produce.Add("kiwifruits", "fruit");
            produce.Add("banana", "fruit");
            produce.Add("bananas", "fruit");
            produce.Add("pear", "fruit");
            produce.Add("pears", "fruit");
            produce.Add("dates", "fruit");
            produce.Add("apple", "fruit");
            produce.Add("apples", "fruit");

            produce.Add("cucumber", "vegetable");
            produce.Add("cucumbers", "vegetable");
            produce.Add("lettuce", "vegetable");
            produce.Add("leek", "vegetable");
            produce.Add("leeks", "vegetable");
            produce.Add("celery", "vegetable");
            produce.Add("carrot", "vegetable");
            produce.Add("carrots", "vegetable");
            produce.Add("potato", "vegetable");
            produce.Add("potatos", "vegetable");
            produce.Add("potatoes", "vegetable");
            produce.Add("cabbage", "vegetable");
            produce.Add("cabbages", "vegetable");

            if (produce.ContainsKey(check))
            {
                output = $"you are in luck! we do have {check} in stock";
            }

            else
            {
                output = $"sorry my man we don't have any {check} today";
            }

            return output;
        }
    }
}
