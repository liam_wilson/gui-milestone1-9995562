﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_04_console
{
    class Program
    {
        static string q1 = "Hello there welcome to the fruit and vege market!";
        static string q2 = "Tell me what you want and I'll check if we have it.";
        static void Main(string[] args)
        {
            while (true)
            {
                var check = "";
                var choose = "";

                Console.Clear();

                Console.WriteLine(q1);
                Console.WriteLine(q2);
                check = Console.ReadLine();

                Console.Clear();

                Console.WriteLine(illcheckman(check));
                Console.WriteLine("type '0' to check another item or press enter to go home");
                choose = Console.ReadLine();

                if (choose == "0")
                {

                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Okay we'll seeya next time!");
                    break;
                }
            }
            
        }

        static string illcheckman(string check)
        {
            var output = "";
            var produce = new Dictionary<string, string>();

            produce.Add("mandarin", "fruit");
            produce.Add("mandarins", "fruit");
            produce.Add("mango", "fruit");
            produce.Add("mangos", "fruit");
            produce.Add("mangoes", "fruit");
            produce.Add("kiwifruit", "fruit");
            produce.Add("kiwifruits", "fruit");
            produce.Add("banana", "fruit");
            produce.Add("bananas", "fruit");
            produce.Add("pear", "fruit");
            produce.Add("pears", "fruit");
            produce.Add("dates", "fruit");
            produce.Add("apple", "fruit");
            produce.Add("apples", "fruit");

            produce.Add("cucumber", "vegetable");
            produce.Add("cucumbers", "vegetable");
            produce.Add("lettuce", "vegetable");
            produce.Add("leek", "vegetable");
            produce.Add("leeks", "vegetable");
            produce.Add("celery", "vegetable");
            produce.Add("carrot", "vegetable");
            produce.Add("carrots", "vegetable");
            produce.Add("potato", "vegetable");
            produce.Add("potatos", "vegetable");
            produce.Add("potatoes", "vegetable");
            produce.Add("cabbage", "vegetable");
            produce.Add("cabbages", "vegetable");

            if (produce.ContainsKey(check))
            {
                output = $"you are in luck! we do have {check} in stock";
            }

            else
            {
                output = $"sorry my man we don't have any {check}today";
            }

            return output;
        }
    }
}
