﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace task_02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "enter the prices of grocery items, your total will be displayed when you press add.";
        static string q2 = "you can keep adding prices and the total will keep going up";
        static double price = 0.00;
        static double total = 0.00;

        public MainPage()
        {
            this.InitializeComponent();
            question1.Text = q1;
            question2.Text = q2;
        }

        private void additem_Click(object sender, RoutedEventArgs e)
        {
            price = double.Parse(ui.Text);
            answer.Text = addingtototal(price);
        }

        static string addingtototal(double price)
        {
            var answer = "";
            total = total + price;
            answer = $"The total cost is ${addGst(total)}";
            return answer;
        }

        static string addGst(double total)
        {
            var output = "";
            total = total * 1.15;
            total = System.Math.Round(total, 2);
            output = Convert.ToString(total);
            return output;
        }

        
    }
}
