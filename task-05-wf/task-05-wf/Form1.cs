﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_05_wf
{
    public partial class Form1 : Form
    {
        static int points = 0;
        static int random = 0;
        static int turns = 0;

        static string info1 = "Guess a number between 1 and 5";
        static string info2 = "you get 5 turns, see how many you can guess";
        static string q1 = "Enter your guess";

        public Form1()
        {
            InitializeComponent();

            in1.Text = info1;
            in2.Text = info2;
            question1.Text = q1;
        }

        private void check_Click(object sender, EventArgs e)
        {
            if (turns < 4)
            {
                output.Text = correctornot(int.Parse(ui.Text));
                score.Text = $"you have a score of {points} well done";
            }
            else
            {
                check.Visible = false;
                output.Text = correctornot(int.Parse(ui.Text));
                score.Text = $"you got a score of {points} well done";
            }
            
        }

        static int randomnum(int random)
        {
            Random rnd = new Random();
            int answer = rnd.Next(1, 6);

            return answer;
        }

        static string correctornot(int check)
        {
            var output = "";
            var rand = randomnum(random);

            if (randomnum(random) == check)
            {
                points = points + 1;
                output = $"you guessed {check} and it was {randomnum(random)}.";
                
            }
            else
            {
                output = $"you guessed {check} and it was {randomnum(random)}.";
            }
            turns++;
            return output;


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
