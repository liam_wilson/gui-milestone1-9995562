﻿namespace task_05_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.in1 = new System.Windows.Forms.Label();
            this.in2 = new System.Windows.Forms.Label();
            this.question1 = new System.Windows.Forms.Label();
            this.ui = new System.Windows.Forms.TextBox();
            this.check = new System.Windows.Forms.Button();
            this.output = new System.Windows.Forms.Label();
            this.score = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // in1
            // 
            this.in1.AutoSize = true;
            this.in1.Location = new System.Drawing.Point(13, 13);
            this.in1.Name = "in1";
            this.in1.Size = new System.Drawing.Size(35, 13);
            this.in1.TabIndex = 0;
            this.in1.Text = "label1";
            // 
            // in2
            // 
            this.in2.AutoSize = true;
            this.in2.Location = new System.Drawing.Point(13, 30);
            this.in2.Name = "in2";
            this.in2.Size = new System.Drawing.Size(35, 13);
            this.in2.TabIndex = 1;
            this.in2.Text = "label2";
            // 
            // question1
            // 
            this.question1.AutoSize = true;
            this.question1.Location = new System.Drawing.Point(13, 47);
            this.question1.Name = "question1";
            this.question1.Size = new System.Drawing.Size(35, 13);
            this.question1.TabIndex = 2;
            this.question1.Text = "label3";
            // 
            // ui
            // 
            this.ui.Location = new System.Drawing.Point(104, 44);
            this.ui.Name = "ui";
            this.ui.Size = new System.Drawing.Size(100, 20);
            this.ui.TabIndex = 3;
            // 
            // check
            // 
            this.check.Location = new System.Drawing.Point(256, 39);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(75, 28);
            this.check.TabIndex = 4;
            this.check.Text = "guess";
            this.check.UseVisualStyleBackColor = true;
            this.check.Click += new System.EventHandler(this.check_Click);
            // 
            // output
            // 
            this.output.AutoSize = true;
            this.output.Location = new System.Drawing.Point(62, 102);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(0, 13);
            this.output.TabIndex = 5;
            // 
            // score
            // 
            this.score.AutoSize = true;
            this.score.Location = new System.Drawing.Point(65, 133);
            this.score.Name = "score";
            this.score.Size = new System.Drawing.Size(0, 13);
            this.score.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 192);
            this.Controls.Add(this.score);
            this.Controls.Add(this.output);
            this.Controls.Add(this.check);
            this.Controls.Add(this.ui);
            this.Controls.Add(this.question1);
            this.Controls.Add(this.in2);
            this.Controls.Add(this.in1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label in1;
        private System.Windows.Forms.Label in2;
        private System.Windows.Forms.Label question1;
        private System.Windows.Forms.TextBox ui;
        private System.Windows.Forms.Button check;
        private System.Windows.Forms.Label output;
        private System.Windows.Forms.Label score;
    }
}

