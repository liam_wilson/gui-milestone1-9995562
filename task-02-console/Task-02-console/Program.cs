﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_02_console
{
    class Program
    {
        static string q1 = "enter the prices of grocery items";
        static string q2 = "do you want to: [0]add more items or [1] finish and get a total price";
        static double price = 0.00;
        static double total = 0.00;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine(q1);
                price = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine(q2);
                var choice = int.Parse(Console.ReadLine());

                Console.WriteLine($"{addingtototal(price, choice)}");
                if (choice == 1)
                {
                    break;
                }
            }
        }


        static string addingtototal(double price, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    total = total + price;
                    break;

                case 1:
                    total = total + price;
                    answer = $"The total cost is ${addGst(total)}";
                    break;

                default:
                    answer = $"Not a valid value was given";
                    break;
            }     
            return answer;
        }

        static string addGst(double total)
        {
            var output = "";
            total = total * 1.15;
            total = System.Math.Round(total, 2);
            output = Convert.ToString(total);
            return output;
        }
    }
}
