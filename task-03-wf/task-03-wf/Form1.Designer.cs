﻿namespace task_03_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.question1 = new System.Windows.Forms.Label();
            this.option = new System.Windows.Forms.ComboBox();
            this.answer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // question1
            // 
            this.question1.AutoSize = true;
            this.question1.Location = new System.Drawing.Point(35, 13);
            this.question1.Name = "question1";
            this.question1.Size = new System.Drawing.Size(35, 13);
            this.question1.TabIndex = 0;
            this.question1.Text = "label1";
            // 
            // option
            // 
            this.option.FormattingEnabled = true;
            this.option.Items.AddRange(new object[] {
            "animals",
            "cars",
            "food",
            "linux distros"});
            this.option.Location = new System.Drawing.Point(38, 57);
            this.option.Name = "option";
            this.option.Size = new System.Drawing.Size(121, 21);
            this.option.TabIndex = 2;
            this.option.SelectedIndexChanged += new System.EventHandler(this.choice_SelectedIndexChanged);
            // 
            // answer
            // 
            this.answer.AutoSize = true;
            this.answer.Location = new System.Drawing.Point(38, 141);
            this.answer.Name = "answer";
            this.answer.Size = new System.Drawing.Size(0, 13);
            this.answer.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 289);
            this.Controls.Add(this.answer);
            this.Controls.Add(this.option);
            this.Controls.Add(this.question1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label question1;
        private System.Windows.Forms.ComboBox option;
        private System.Windows.Forms.Label answer;
    }
}

