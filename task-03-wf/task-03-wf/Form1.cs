﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_03_wf
{
    public partial class Form1 : Form
    {
        static string q1 = "please select what list of things you would like to look at.";

        public Form1()
        {
            InitializeComponent();
            question1.Text = q1;
        }

        private void choice_SelectedIndexChanged(object sender, EventArgs e)
        {
            answer.Text = list(option.SelectedIndex);
        }

        static string list(int choice)
        {
            var output = "";

            switch (choice)
            {
                case 0:
                    output = "cat, dog, horse, gorilla";
                    break;
                case 1:
                    output = "nissan tiida, nissan skyline, nissan pulsar, subaru impreza";
                    break;
                case 2:
                    output = "black beans, garlic cheese toasty, noodles, meatball sub of the day";
                    break;
                case 3:
                    output = "gentoo, debian, arch, mint";
                    break;
                default:
                    break;
            }
            return output;

        }
    }
}
