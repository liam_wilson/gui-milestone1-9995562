﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(q1);
            var units = Console.ReadLine();

            Console.WriteLine(q2);
            var choice = int.Parse(Console.ReadLine());

            Console.WriteLine($"{choices(units, choice)}");

            Console.WriteLine($"Press <ENTER> when you are done");
            Console.ReadLine();
        }

        static string q1 = "enter the amount of distance you want to convert";
        static string q2 = "Do you want to convert it to [0]miles or [1]kilometers?";

        static string choices(string units, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"{units} Kilometers = {convertmile(double.Parse(units))} Miles";
                    break;
                case 1:
                    answer = $"{units} Miles = {convertkm(double.Parse(units))} Kilometers";
                    break;
                default:
                    answer = $"Not a valid value was given";
                    break;
            }

            return answer;
        }

        static double convertmile(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var output = mile * converter;

            output = System.Math.Round(output, 2);
            return output;
        }

        static double convertkm(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var output = km * converter;

            output = System.Math.Round(output, 2);
            return output;
        }

    }
}
