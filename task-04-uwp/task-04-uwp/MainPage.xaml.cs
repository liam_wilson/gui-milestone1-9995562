﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "Hello there welcome to the fruit and vege market!";
        static string q2 = "Tell me what you want and I'll check if we have it.";

        public MainPage()
        {
            this.InitializeComponent();
            question1.Text = q1;
            question2.Text = q2;
        }

        private void check_Click(object sender, RoutedEventArgs e)
        {
            answer.Text = illcheckman(ui.Text);
        }
        static string illcheckman(string check)
        {
            var output = "";
            var produce = new Dictionary<string, string>();

            produce.Add("mandarin", "fruit");
            produce.Add("mandarins", "fruit");
            produce.Add("mango", "fruit");
            produce.Add("mangos", "fruit");
            produce.Add("mangoes", "fruit");
            produce.Add("kiwifruit", "fruit");
            produce.Add("kiwifruits", "fruit");
            produce.Add("banana", "fruit");
            produce.Add("bananas", "fruit");
            produce.Add("pear", "fruit");
            produce.Add("pears", "fruit");
            produce.Add("dates", "fruit");
            produce.Add("apple", "fruit");
            produce.Add("apples", "fruit");

            produce.Add("cucumber", "vegetable");
            produce.Add("cucumbers", "vegetable");
            produce.Add("lettuce", "vegetable");
            produce.Add("leek", "vegetable");
            produce.Add("leeks", "vegetable");
            produce.Add("celery", "vegetable");
            produce.Add("carrot", "vegetable");
            produce.Add("carrots", "vegetable");
            produce.Add("potato", "vegetable");
            produce.Add("potatos", "vegetable");
            produce.Add("potatoes", "vegetable");
            produce.Add("cabbage", "vegetable");
            produce.Add("cabbages", "vegetable");

            if (produce.ContainsKey(check))
            {
                output = $"you are in luck! we do have {check} in stock";
            }

            else
            {
                output = $"sorry my man we don't have any {check} today";
            }

            return output;
        }
    }
}
